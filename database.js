const mongoose = require("mongoose");

mongoose.connect(process.env.MONGO_URL,{
    useCreateIndex:true,
    useNewUrlParser:true,
    useFindAndModify:false,
    useUnifiedTopology: true 
})
.then(db=>console.log("connect with local db"))
.catch(e=>console.log("error in connect with mongo localdb"));
