const mongoose= require('mongoose');
const {Schema} = mongoose;

const user_data= new Schema ({
    user_name:{type:String},
    age:{type:String},
    favorite_food:{type:String},
})


module.exports= mongoose.model("user_data",user_data);
